import React, { Component } from 'react'

class Message extends Component {

	constructor() {
		super()
		this.state = {
			message: 'Edit'
		}
	}

	changeMessage(){
		this.setState({
			message: 'done'
		})
	}

	render() {
		return (
			<div>
			<h1>{this.state.message}</h1>
			<button onClick={() => this.changeMessage()} >Ya</button>
			</div>
		) 
	}
}
export default Message